# OfekDaraby.com ![project-icon](public/favicon.ico)
Files for the small, experimental site I made - [ofekdaraby.com](ofekdaraby.com)

## Description
These are just the source files from a project i'm working on.
I wanted to make them public so everyone can check them out and perhaps even draw inspiration or use them in their own way in different projects.
The files come ready in a React project structure, you can copy them to a newly made React project and run `$npm install` in order to install all the required dependencies before running it for yourself.
Not all the code was written by me, some parts (such as SplitText.js) were taken from sources around the web. In these cases I did credit the author and linked to the source, as well as changed the code to suit my needs from it.

## License
All the code that was written by me here is free for anyone to use in any way they want, without needing to credit me or even inform me.
Be aware that some assets, such as the fonts and images are liceneced so if you created a product for commercial purpose using them, you will need to aquire the rights to use them.

## Project status
I'm currently continuing to develop and add to the project, any major changes made to the development project will be added here as well.
If you have any suggestions or questions, you can let me know by opening an issue or contacting me via my email.


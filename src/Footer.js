import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

export default function Footer() {
  return (
    <Box className="footer-box">
      <hr />
      <Typography variant="body2" align="center">
        {"© "}
        {new Date().getFullYear()}
        {" Ofek Daraby."}
      </Typography>
    </Box>
  );
}

import React from "react";

import titleUnderline from "./images/title underline.png";
import arrow from "./images/arrow.png";

import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";

export default class StyleTool extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toCSS: true,
      cssText: "",
      jsText: "",
      condense: false,
      matchSpaces: false,
    };

    this.makeConversion = this.makeConversion.bind(this);
    this.isEndOfLine = this.isEndOfLine.bind(this);
  }

  /*
  A function to check if the style's special character indicates the end of a line or not (';' for css, ',' for js)
  The function does relay on conventional syntax
  */
  isEndOfLine(text, currentIndex) {
    let afterSpaces = false;
    for (let i = currentIndex; i < text.length; i++) {
      if (
        afterSpaces &&
        (text[i] === ":" ||
          (afterSpaces && text[i] === " ") ||
          text[currentIndex] === "\t")
      ) {
        // if ":", it means the line did end earlier, if it's another space or tab, it hasen't
        return text[i] === ":";
      } else if (text[i] !== " " && text[i] !== "\t") {
        // if spaces/tabs have ended
        afterSpaces = true;
      }
    }
    // if reached the end of the text, it's the end of the line
    return true;
  }

  /*
  A function to convert the input text from css to js or the other way around
  Allows for options such as condencing to a single line or matching spaces / tabs across all lines
  */
  makeConversion() {
    let convertedText = "";
    let atPropety = false;

    const conversionText = this.state.toCSS
      ? this.state.jsText.replaceAll("\n", "")
      : this.state.cssText.replaceAll("\n", "");

    if (conversionText.replaceAll(" ", "").replaceAll("\t", "").length) {
      if (this.state.toCSS) {
        // converting to css
        for (let index in conversionText) {
          const char = conversionText[index];

          // checking for start/end of property
          if (char === ":") {
            atPropety = true;
          }

          // checking for characters that need to be replaced
          if (
            !atPropety &&
            char.toLowerCase() !== char.toUpperCase() &&
            char === char.toUpperCase()
          ) {
            convertedText += "-" + char.toLowerCase();
          } else if (
            char === "," &&
            this.isEndOfLine(conversionText, index + 1)
          ) {
            convertedText += ";\n";
            atPropety = false;
          } else if (!(char === "'" || char === '"')) {
            convertedText += char;
          }
        }
        // adding line break if necessary
        if (convertedText[convertedText.length - 1] !== "\n") {
          convertedText += ";";
        }
      } else {
        // converting to js
        let skipChar = false;
        for (let index in conversionText) {
          const char = conversionText[index];

          if (skipChar) {
            skipChar = false;
          } else {
            // checking for special characters
            if (char === ":") {
              if (atPropety) {
                convertedText += ":";
              } else if (index < conversionText.length - 2) {
                // adding the quotes
                convertedText += ': "';
                if (conversionText[++index] === " ") {
                  skipChar = true;
                }
                atPropety = true;
              }
            } else if (
              !atPropety &&
              char === "-" &&
              index < conversionText.length - 2
            ) {
              skipChar = true;
              index++;
              convertedText += conversionText[index].toUpperCase();
            } else if (char === ";") {
              // checking for end of line
              if (this.isEndOfLine(conversionText, index + 1)) {
                convertedText += '",\n';
                atPropety = false;
              } else {
                convertedText += ",";
              }
            } else {
              convertedText += char;
            }
          }
        }
        // removing last char if it's a space - fixes a problem
        if (convertedText[convertedText.length - 1] === " ") {
          convertedText = convertedText.substr(0, convertedText.length - 1);
        }
        // adding line break if necessary
        if (convertedText[convertedText.length - 1] !== "\n") {
          convertedText += '"';
        }
      }

      // Middlewares (from the options next to the "Convert" button)
      if (this.state.matchSpaces) {
        convertedText.replace("\t", "    ");

        // getting amount of spaces in first line
        let spaces = "";
        for (
          let i = 0;
          i < convertedText.length && convertedText[i] === " ";
          i++
        ) {
          spaces += " ";
        }

        // replacing all spaces blocks in the other lines
        let oldSpaces = 0;
        let track = true;

        for (let i = 0; i < convertedText.length; i++) {
          // if first non-space, slice the converted text
          if (convertedText[i] !== " " && track) {
            convertedText =
              convertedText.slice(0, oldSpaces) +
              spaces +
              convertedText.slice(i);
            track = false;
          }
          // if at line drop, start traking again
          if (convertedText[i] === "\n") {
            oldSpaces = i + 1;
            track = true;
          }
        }
      } else if (this.state.condense) {
        // splitting the text at ": " (to keep it looking good at these parts, not super important)
        convertedText = convertedText.split(": ");

        // for each part - trim the tabs, spaces, and line drops
        for (let i = 0; i < convertedText.length; i++) {
          convertedText[i] = convertedText[i]
            .replaceAll("\t", "")
            .replaceAll(" ", "")
            .replaceAll("\n", " ");
        }

        // join all parts again with the same ": "
        convertedText = convertedText.join(": ");
      }

      // updating the state
      if (this.state.toCSS) {
        this.setState({ cssText: convertedText });
      } else {
        this.setState({ jsText: convertedText });
      }
    }
  }

  render() {
    return (
      <Container maxWidth="xl" className="siteBody">
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
          flexWrap="wrap"
          marginTop="25px"
          marginBottom="45px"
          className="title-section"
        >
          <img
            src={titleUnderline}
            alt=""
            width={800}
            height={40}
            className="dissapear-at-width"
          />
          <h1 className="head-title">JS to CSS</h1>
          <img
            src={titleUnderline}
            alt=""
            width={800}
            height={40}
            className="dissapear-at-width"
          />
        </Box>
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
          display="flex"
        >
          <Grid item align="center" style={{ minWidth: "30%" }}>
            <h1
              className="head-title"
              style={{ fontSize: "2rem", lineHeight: "1" }}
            >
              JS - React
            </h1>
            <textarea
              rows="18"
              placeholder={
                'For example:\n\tcolor: "white",\n\tbackgroundColor: "DodgerBlue",\n\tpadding: "10px",\n\tfontFamily: "Arial"'
              }
              spellcheck="false"
              value={this.state.jsText}
              onChange={(e) => this.setState({ jsText: e.target.value })}
              className={this.state.toCSS ? "not-selected" : "selected"}
            />
          </Grid>
          <Grid
            direction="column"
            alignItems="center"
            justifyContent="center"
            align="center"
          >
            <Grid item>
              <img
                src={arrow}
                alt=""
                width={250}
                height={125}
                className={
                  this.state.toCSS
                    ? "dissapear-at-width js-arrow"
                    : "dissapear-at-width css-arrow"
                }
              />
            </Grid>
            <Grid item>
              <button
                className="switch-button"
                onClick={(e) => this.setState({ toCSS: !this.state.toCSS })}
              >
                Switch!
              </button>
            </Grid>
          </Grid>
          <Grid align="center" style={{ minWidth: "30%" }}>
            <h1
              className="head-title"
              style={{ fontSize: "2rem", lineHeight: "1" }}
            >
              CSS
            </h1>
            <textarea
              value={this.state.cssText}
              rows="18"
              placeholder={
                "For example:\n\tcolor: white;\n\tbackground-color: DodgerBlue;\n\tpadding: 10px;\n\tfont-family: Arial"
              }
              spellcheck="false"
              onChange={(e) => this.setState({ cssText: e.target.value })}
              className={this.state.toCSS ? "selected" : "not-selected"}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} align="center">
          <input
            type="checkbox"
            checked={this.state.matchSpaces}
            onChange={(e) =>
              this.setState({ matchSpaces: !this.state.matchSpaces })
            }
            disabled={this.state.condense}
          />
          <label>Match tabs / Spaces (of first line)</label>
          <input
            type="checkbox"
            checked={this.state.condense}
            onChange={(e) => this.setState({ condense: !this.state.condense })}
            disabled={this.state.matchSpaces}
          />
          <label>Condense to a single line</label>
          <button className="convert-button" onClick={this.makeConversion}>
            Convert
          </button>
        </Grid>
      </Container>
    );
  }
}

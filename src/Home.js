import React from "react";

import SplitText from "./SplitText";

import Box from "@mui/material/Box";

/*
All the marks' grids for all the letters (plus space)
*/
const lettersGrids = {
  A: [" ##   ", "#  #  ", "####  ", "#  #  ", "#  #  "],
  B: ["###   ", "#  #  ", "###   ", "#  #  ", "###   "],
  C: ["####  ", "#     ", "#     ", "#     ", "####  "],
  D: ["###   ", "#  #  ", "#  #  ", "#  #  ", "###   "],
  E: ["####  ", "#     ", "####  ", "#     ", "####  "],
  F: ["####  ", "#     ", "###   ", "#     ", "#     "],
  G: ["####  ", "#     ", "# ##  ", "#  #  ", "####  "],
  H: ["#  #  ", "#  #  ", "####  ", "#  #  ", "#  #  "],
  I: ["### ", " #  ", " #  ", " #  ", "### "],
  J: ["####  ", "   #  ", "#  #  ", "#  #  ", " ##   "],
  K: ["# ##  ", "###   ", "###   ", "# ##  ", "#  ## "],
  L: ["#     ", "#     ", "#     ", "#     ", "####  "],
  M: ["#   # ", "## ## ", "##### ", "# # # ", "#   # "],
  N: ["#  #  ", "## #  ", "####  ", "# ##  ", "#  #  "],
  O: ["#### ", "#  # ", "#  # ", "#  # ", "#### "],
  P: ["###   ", "#  #  ", "###   ", "#     ", "#     "],
  Q: [" ##   ", "#  #  ", "#  #  ", "# #   ", " # #  "],
  R: ["###   ", "#  #  ", "###   ", "#  #  ", "#  #  "],
  S: [" ###  ", "#     ", " ##   ", "   #  ", "###   "],
  T: ["##### ", "  #   ", "  #   ", "  #   ", "  #   "],
  U: ["#  #  ", "#  #  ", "#  #  ", "#  #  ", " ##   "],
  V: ["#   # ", "#   # ", " # #  ", " # #  ", "  #   "],
  W: ["#   # ", "# # # ", "##### ", "## ## ", "#   # "],
  X: ["#  #  ", "#  #  ", " ##   ", "#  #  ", "#  #  "],
  Y: ["#   # ", " # #  ", "  #   ", "  #   ", "  #   "],
  Z: ["####  ", "  ##  ", " ##   ", "##    ", "####  "],
  " ": ["      ", "      ", "      ", "      ", "      "],
};

const subtextOptions = [
  "PROGRAMMING",
  "GAME DEVELOPMENT",
  "FULL STACK",
  "WOW",
  "SUCH COOL",
  "MUCH ANIMATIONS",
];

const maxAnimationColor = 2;

export default class AboutMe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: 0,
      showSubtext: true,
    };

    this.makeGrid = this.makeGrid.bind(this);
    this.randomColorize = this.randomColorize.bind(this);
  }

  /*
  After component mount - set two intervals to switch every n seconds the subtitle
  one interval to spawn to switch the current subtext and another to clear it (to make it consistent and not glitch)
  */
  componentDidMount() {
    const OFFSET_TIME = 3800;
    const ANIMATION_TIME = 4000;
    const PARTICLES_DELAY = 1;
    const STARTING_ANIMATION_TIME = 1000;

    // change the subtext every 4 seconds
    this.interval = setInterval(
      () =>
        this.setState({
          selectedOption:
            this.state.selectedOption + 1 === subtextOptions.length
              ? 0
              : this.state.selectedOption + 1,
          showSubtext: true,
        }),
      ANIMATION_TIME
    );

    // first wait 1 second (to allow starting animation to finish)
    new Promise((r) => setTimeout(r, STARTING_ANIMATION_TIME)).then(() => {
      // randomly colorizing pixels
      this.particlesInterval = setInterval(() => {
        if (!this.randomColorize()) {
          clearInterval(this.particlesInterval);
        }
      }, PARTICLES_DELAY);
    });

    // first wait 3.8 seconds (to not sync with the first interval)
    new Promise((r) => setTimeout(r, OFFSET_TIME)).then(() => {
      // clear the subtext every 4 seconds
      this.interval2 = setInterval(
        () =>
          this.setState({
            showSubtext: false,
          }),
        ANIMATION_TIME
      );
      // for the first clear
      this.setState({
        showSubtext: false,
      });
    });
  }

  /*
  Before unmounting - clear the intervals
  */
  componentWillUnmount() {
    clearInterval(this.interval);
    clearInterval(this.interval2);
    clearInterval(this.particlesInterval);
  }

  /*
  A function to pick a random (visible) block from the name (- has the "grid-size2" class) and assaign it a random color's animation
  tried applying to the subtext - creates weird effects due to the opacity: 1 change but can't get rid of it or the text starts disappearing
  */
  randomColorize() {
    // chosing a random block (that meats th requirements)
    let chosenBlock = document.getElementsByClassName(
      "grid-block-visible grid-size2"
    )[
      Math.floor(
        Math.random() *
          document.getElementsByClassName("grid-block-visible grid-size2")
            .length
      )
    ];

    if (!chosenBlock) {
      return false;
    }
    // updateing it's style
    chosenBlock.style.animation =
      "colorize-" +
      String(Math.floor(Math.random() * maxAnimationColor) + 1) +
      " 1s";
    chosenBlock.style.opacity = "1";
    return true;
  }

  /*
  A function to make a given text's divs grid - allows to change the multiplier of the letters (1x1, 2x2... for every point)
  Order of loops:
  1 - for each letter, make an inline-block styled div
  2 - for each line in the letters list
  3 - for each iteration of the multiplier - in order to double the rows (places a div at the end to seperate)
  4 - for every char in the letter's line
  5 - for each iteration of the multiplier - in order to double the columns
  then add the div with classes that correlate to the current char
  */
  makeGrid(text, multiplier) {
    return (
      <>
        {text.split("").map((letter) => {
          return (
            <div style={{ display: "inline-block" }}>
              {lettersGrids[letter].map((line) => {
                return (
                  <>
                    {Array.from(Array(multiplier).keys()).map(() => {
                      return (
                        <>
                          {line.split("").map((char, charIndex) => {
                            return (
                              <>
                                {Array.from(Array(multiplier).keys()).map(
                                  () => {
                                    const divClass =
                                      char === "#"
                                        ? "grid-block grid-block-visible grid-size" +
                                          String(multiplier)
                                        : "grid-block grid-block-invisible grid-size" +
                                          String(multiplier);
                                    return (
                                      <div
                                        className={divClass}
                                        style={{
                                          animationDelay: charIndex / 8 + "s",
                                        }}
                                      />
                                    );
                                  }
                                )}
                              </>
                            );
                          })}
                          <div />
                        </>
                      );
                    })}
                  </>
                );
              })}
            </div>
          );
        })}
      </>
    );
  }

  render() {
    console.log(this.state.showSubtext);
    return (
      <>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
          height="calc(100vh - 100px)"
        >
          <div className="dissapear-at-width">
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              flexDirection="column"
              className="dissapear-at-widtha"
            >
              <div>{this.makeGrid("OFEK", 2)}</div>
              <p />
              <div>
                {this.makeGrid(" ", 1)}
                {this.state.showSubtext
                  ? this.makeGrid(subtextOptions[this.state.selectedOption], 1)
                  : null}
                {this.makeGrid(" ", 1)}
              </div>
            </Box>
          </div>
          <div className="appear-at-width">
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              flexDirection="column"
            >
              <h1 style={{ fontSize: "54px", opacity: "1" }}>
                <SplitText copy="OFEK" role="heading" spansClass="intro-only" />
              </h1>
              <h1>
                {this.state.showSubtext ? (
                  <SplitText
                    copy={subtextOptions[this.state.selectedOption]}
                    role="heading"
                    spansClass="intro-outro"
                  />
                ) : (
                  <span>&nbsp;</span>
                )}
              </h1>
            </Box>
          </div>
        </Box>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          height="calc(20px)"
          color="#9a9a9a"
        >
          <label>
            All the code for this site can be found on Gitlab{" "}
            <a href="https://gitlab.com/Ofekdar" style={{ color: "#9a9a9a" }}>
              here
            </a>
          </label>
        </Box>
      </>
    );
  }
}

import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import AnimatedImage from "./AnimatedImage";
import React, { useState, useEffect } from "react";

/*
A function to turn a hsl color (the h value) to hex (returns as a string)
the s and l are constants and can be changed to aquire different color schemes
credit to icl7126 (2020) : https://stackoverflow.com/questions/36721830/convert-hsl-to-rgb-and-hex
*/
function hslToHex(h) {
  // making the parameters needed for the convertion
  const s = 100;
  const l = 70 / 100;
  const a = (s * Math.min(l, 1 - l)) / 100;
  const f = (n) => {
    const k = (n + h / 30) % 12;
    const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
    return Math.round(255 * color)
      .toString(16)
      .padStart(2, "0"); // convert to Hex and prefix "0" if needed
  };

  // making the hex color using the f function variable
  return `#${f(0)}${f(8)}${f(4)}b2`;
}

/*
A function to make the label to be displayed on the cards that changes colors on every mouse hover
the color is random from the hsl table to make the color always bright and have a defined color (cant be dark color like brown and black while cant being too bright or white)
*/
function displayButton(value) {
  return (
    <button
      className="card-label"
      style={{ backgroundColor: "#88f" }}
      onMouseEnter={(e) =>
        // changing the color of the button on hover, doing it like this makes it a different color every time hovered
        // you can just pass the function without the "(e) =>" to make it the same color until page reload
        (e.target.style.backgroundColor = hslToHex(
          //Math.ceil(Math.floor(Math.random() * 360) / 60.0) * 60 // For rounding to 60 (6 basic colors)
          Math.floor(Math.random() * 360) // for the entire range of colors (360 colors)
        ))
      }
      onMouseLeave={(e) => (e.target.style.backgroundColor = "#88f")}
    >
      {value}
    </button>
  );
}

export default function InfoCard(props) {
  const [visible, setVisible] = useState(false);

  /*
  A function to handle when the user scrolls with the mouse to change the animated image's visibility
  */
  const handleScroll = () => {
    const element = document.getElementById(props.title[0]);

    // checking if the image is on the screen and should be rendered
    if (
      !visible &&
      element.getBoundingClientRect().y > 50 &&
      element.getBoundingClientRect().y + element.clientHeight / 2 <
        window.innerHeight
    ) {
      setVisible(true);
    }

    /*
    Uncomment to make the image disappear again once it left the screen
    It will play the animation again once it goes back on screen

    // checking if the image isn't on the screen and shouldn't be rendered
    else if (
      visible &&
      (element.getBoundingClientRect().y + element.clientHeight - 40 < 0 ||
        element.getBoundingClientRect().y > window.innerHeight)
    ) {
      setVisible(false);
    }
    */
  };

  // for the first time the function runs - add scroll event listeners for the images and check once if the image is on screen
  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });
    handleScroll();

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  return (
    <Paper
      sx={{
        backgroundColor: "rgba(255, 255, 255, 0.3)",
        color: "#ededed",
        mb: 4,
      }}
      className="card-paper"
      id={props.title[0]}
    >
      <Grid container justifyContent="center">
        {/* the animated image place in the grid */}
        <div className="animated-image-div dissapear-at-width">
          {/* the first image makes sure the space isn't empty when the animated image has dispaly: "none" */}
          <img
            width={props.image.sizes[0]}
            height={props.image.sizes[1]}
            alt=""
            src={null}
            className="image-placeholder"
          />
          <AnimatedImage
            image={props.image.source}
            pieces={12}
            sizes={props.image.sizes}
            blit={visible}
          />
        </div>
        {/* the line in the middle and the title with the labels */}
        <div className="card-line dissapear-at-width"></div>
        <Grid item md={6} xs={8}>
          <Box
            sx={{
              position: "relative",
              p: { xs: 3, md: 4 },
              pr: { md: 0 },
            }}
            className="labels-box"
          >
            <h2 className="card-title">{props.title}</h2>
            {props.labels.map((label) => {
              // mapping all the labels
              // if the label has a title (then it's another object), make all the subtitles with their labels
              if (label.title) {
                return (
                  <>
                    <Typography
                      component="h3"
                      variant="h5"
                      color="inherit"
                      gutterBottom
                    >
                      {label.title}
                    </Typography>
                    {label.labels.map((sublabel) => displayButton(sublabel))}
                  </>
                );
              } else {
                return displayButton(label);
              }
            })}
          </Box>
        </Grid>
      </Grid>
    </Paper>
  );
}

import Grid from "@mui/material/Grid";

export default function AnimatedImage(props) {
  /*
  Props:
  props.image (image source) - the image source that will be divided
  props.pieces (int 1<=x<=12) - the amount of pieces to divide the image to
  props.sizes (list [x, y]) - the sizes of the ORIGINAL PICTURE - must be the sizes of the source image or else it will look weird
  */

  /*
  dividing the image to pieces by positions, delays and keys (ids)
  */
  var picArr = [];
  for (let i = 0; i < props.pieces; i++) {
    for (let j = 0; j < props.pieces; j++) {
      picArr.push({
        offset:
          ((j * 100) / (props.pieces - 1)).toString() +
          "% " +
          ((i * 100) / (props.pieces - 1)).toString() +
          "%",
        delayIndex: (i + j) / 15,
        blur: i && j && i !== props.pieces - 1 && j !== props.pieces - 1,
        id: i + "-" + j,
      });
    }
  }

  return (
    <Grid
      container
      maxWidth={props.sizes[0]}
      maxHeight={props.sizes[0]}
      spacing={0}
      className={"animated-image"}
      style={{ display: props.blit ? "inline-flex" : "none" }}
    >
      {/* mapping the array of the cut images */}
      {picArr.map((nextPicInfo) => (
        <Grid
          item
          xs={12 / props.pieces}
          style={{
            marginBottom: "-4px",
          }}
          height={"auto"}
          key={nextPicInfo.id}
        >
          <img
            width={props.sizes[0] / props.pieces}
            height={props.sizes[1] / props.pieces}
            style={{
              objectFit: "none",
              backgroundSize: "100%",
              objectPosition: nextPicInfo.offset,
              animation: nextPicInfo.blur
                ? "appear 0.4s ease-in forwards, blur 4s ease-in forwards"
                : "appear 0.4s ease-in forwards",
              animationDelay: nextPicInfo.blur
                ? nextPicInfo.delayIndex + "s"
                : nextPicInfo.delayIndex / 2 + "s",
            }}
            src={props.image}
            alt=""
          />
        </Grid>
      ))}
    </Grid>
  );
}

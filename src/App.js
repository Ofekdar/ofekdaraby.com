import "./styles/App.css";
import "./styles/Home.css";
import "./styles/InfoCard.css";
import "./styles/AnimatedImage.css";
import "./styles/SpecialProperties.css";
import "./styles/Footer.css";
import "./styles/StyleTool.css";

import ComputerKnowledge from "./ComputerKnowledge";
import StyleTool from "./StyleTool";
import Home from "./Home";
import Footer from "./Footer";

import { Icon } from "@iconify/react";
import arrowLeftBoldHexagonOutline from "@iconify/icons-mdi/arrow-left-bold-hexagon-outline";
import arrowRightBoldHexagonOutline from "@iconify/icons-mdi/arrow-right-bold-hexagon-outline";
import homeOutline from "@iconify/icons-mdi/home-outline";
import xmlIcon from "@iconify/icons-mdi/xml";
import switchHorizontal from "@iconify/icons-heroicons-solid/switch-horizontal";

import * as React from "react";

import { styled, ThemeProvider, createTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(8)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 5px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

const topbarTheme = createTheme({
  palette: {
    primary: {
      main: "#3e3f62",
    },
    icons: {
      main: "#fff",
    },
  },
});

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sidebarOpen: false,
      selectedComponent: <Home />,
    };

    this.toogleSidebarOpen = this.toogleSidebarOpen.bind(this);
    this.changeSelectedComponent = this.changeSelectedComponent.bind(this);
  }

  // A function to toggle if the side bar is open
  toogleSidebarOpen() {
    this.setState({ sidebarOpen: !this.state.sidebarOpen });
  }

  // A function to change the currently selected component to display
  changeSelectedComponent(newComponent) {
    this.setState({ selectedComponent: newComponent });
  }

  render() {
    return (
      <Box display="flex">
        <ThemeProvider theme={topbarTheme}>
          <Drawer
            variant="permanent"
            open={this.state.sidebarOpen}
            PaperProps={{ sx: { backgroundColor: "#362999", color: "white" } }}
          >
            <DrawerHeader
              onClick={this.toogleSidebarOpen}
              style={{
                marginLeft: "auto",
                marginRight: "auto",
                cursor: "pointer",
              }}
            >
              {this.state.sidebarOpen ? (
                <Icon
                  icon={arrowLeftBoldHexagonOutline}
                  color="white"
                  width="32"
                  height="32"
                />
              ) : (
                <Icon
                  icon={arrowRightBoldHexagonOutline}
                  color="white"
                  width="32"
                  height="32"
                />
              )}
            </DrawerHeader>
            <Divider />
            {[
              {
                text: "Home",
                icon: homeOutline,
                linkedComponent: <Home />,
                divider: <Divider />,
              },
              {
                text: "Computer Knowledge",
                icon: xmlIcon,
                linkedComponent: <ComputerKnowledge />,
                divider: <Divider />,
              },
              {
                text: "JS to CSS",
                icon: switchHorizontal,
                linkedComponent: <StyleTool />,
                divider: null,
              },
            ].map((item, index) => (
              <>
                <List>
                  <ListItem
                    button
                    key={item.text}
                    onClick={(e) =>
                      this.changeSelectedComponent(item.linkedComponent)
                    }
                  >
                    <ListItemIcon>
                      <Icon
                        icon={item.icon}
                        color="white"
                        width="32"
                        height="32"
                      />
                    </ListItemIcon>
                    <ListItemText primary={item.text} />
                  </ListItem>
                </List>
                {item.divider}
              </>
            ))}
          </Drawer>
        </ThemeProvider>
        <Box component="main" sx={{ flexGrow: 1 }}>
          {this.state.selectedComponent}
          <Footer />
        </Box>
      </Box>
    );
  }
}

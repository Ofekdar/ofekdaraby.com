var TRANSITION_TIME = 1.0;

/*
A function to split given text into multiple spans in order to allow for animation over each char seperatly
Credit to Sarah Fossheim, original: https://fossheim.io/writing/posts/react-text-splitting-animations/
This version is a bit moddified to make the transition smoother and better
*/
export default function SplitText(props) {
  let notSpaceChars = props.copy.replaceAll(" ", "").length; // to make the transition the same length (in time) no matter the text's length
  let currentSpaces = 0; // to make it look smoother even with spaces
  return (
    <span aria-label={props.copy} role={props.role} className="animatedTitle">
      {props.copy.split("").map(function (char, index) {
        let style = {
          animationDelay:
            0.1 + (TRANSITION_TIME / notSpaceChars) * currentSpaces + "s",
        };
        if (char !== " ") {
          currentSpaces++;
        }
        return (
          <span
            aria-hidden="true"
            key={index}
            style={style}
            className={props.spansClass}
          >
            {char}
          </span>
        );
      })}
    </span>
  );
}

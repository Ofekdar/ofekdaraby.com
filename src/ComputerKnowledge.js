import React from "react";

import coding from "./images/coding.png";
import technologies from "./images/technologies.jpg";
import education from "./images/education.jpg";
import unrealEngine from "./images/unreal engine.jpg";
import titleUnderline from "./images/title underline.png";

import InfoCard from "./InfoCard";

import Box from "@mui/material/Box";
import Container from "@mui/material/Container";

/*
All the cards with the titles and labels
*/
var cards = [
  {
    id: "languages",
    title: "Languages",
    image: { source: coding, sizes: [310, 232] },
    labels: ["C++", "Pythong", "C", "Javascript", "html"],
  },
  {
    id: "technologies",
    title: "Technologies",
    image: { source: technologies, sizes: [480, 270] },
    labels: ["React", "SQL", "Flutter", "Git", "Django", "css", "Bootstrap"],
  },
  {
    id: "education-and-experience",
    title: "Education & Experience",
    image: { source: education, sizes: [373, 194] },
    labels: [
      {
        title: "Highschool",
        labels: ["Mechanical Physics", "Computer Science"],
      },
      {
        title: "Professional",
        labels: [
          "Magshimim - Israel's national cyber education program",
          "Rafael Defense Systems - R&D (2020 - 2022)",
        ],
      },
    ],
  },
  {
    id: "unreal-engine",
    title: "Unreal Engine 5",
    image: { source: unrealEngine, sizes: [360, 201] },
    labels: ["Blueprints", "Landscapes", "Widgets", "Multiplayer"],
  },
];

export default class ComputerKnowledge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container maxWidth="xl" className="siteBody">
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
          flexWrap="wrap"
          marginTop="25px"
          marginBottom="45px"
          className="title-section"
        >
          <img
            src={titleUnderline}
            alt=""
            width={800}
            height={40}
            className="dissapear-at-width"
          />
          <h1 className="head-title">My Computer Knowledge</h1>
          <img
            src={titleUnderline}
            alt=""
            width={800}
            height={40}
            className="dissapear-at-width"
          />
        </Box>
        {cards.map((card, index) => (
          <InfoCard
            image={card.image}
            title={card.title}
            labels={card.labels}
          />
        ))}
      </Container>
    );
  }
}
